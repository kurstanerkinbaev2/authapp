﻿using Blazored.SessionStorage;
using Domain.DTO;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Json;

namespace Front.Services
{
    public class CustomAuthStateProvider : AuthenticationStateProvider
    {
        private readonly ProtectedSessionStorage sessionStorage;
        private ClaimsPrincipal anonymous = new ClaimsPrincipal(new ClaimsIdentity());
        public CustomAuthStateProvider(ProtectedSessionStorage sessionStorage)
        {
            this.sessionStorage = sessionStorage;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            try
            {

                var result = await sessionStorage.GetAsync<string>("token");
                string token = result.Success ? result.Value : null;

                if (string.IsNullOrEmpty(token))
                {
                    return await Task.FromResult(new AuthenticationState(anonymous));
                }

                var identity = new ClaimsIdentity(ParseClaimsFromJwt(token), "jwt");
                var user = new ClaimsPrincipal(identity);
                return await Task.FromResult(new AuthenticationState(user));

            }
            catch
            {
                return await Task.FromResult(new AuthenticationState(anonymous));
            }
        }

        public async Task UpdateAuthenticationState(AuthResponse response)
        {
            ClaimsPrincipal user;

            if (response != null)
            {
                await sessionStorage.SetAsync("token", response.token);
                await sessionStorage.SetAsync("refreshToken", response.refreshToken);
                var identity = new ClaimsIdentity(ParseClaimsFromJwt(response.token), "jwt");
                user = new ClaimsPrincipal(identity);
            }
            else
            {
                await sessionStorage.DeleteAsync("token");
                await sessionStorage.DeleteAsync("refreshToken");
                user = anonymous;
            }

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(user)));
        }

        public async Task<AuthResponse> GetToken()
        {
            var tokenRes = await sessionStorage.GetAsync<string>("token");
            string token = tokenRes.Success ? tokenRes.Value : null;
            var refreshRes = await sessionStorage.GetAsync<string>("refreshToken");
            string refreshToken = refreshRes.Success ? refreshRes.Value : null;

            return new AuthResponse { token = token, refreshToken = refreshToken };
        } 

        public static IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
        {
            var payload = jwt.Split('.')[1];
            var jsonBytes = ParseBase64WithoutPadding(payload);
            var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);
            return keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString()));
        }
        private static byte[] ParseBase64WithoutPadding(string base64)
        {
            switch (base64.Length % 4)
            {
                case 2: base64 += "=="; break;
                case 3: base64 += "="; break;
            }
            return Convert.FromBase64String(base64);
        }
    }
}
