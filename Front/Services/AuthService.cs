﻿using Domain.DTO;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;
using Microsoft.AspNetCore.Http;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Net;
using Microsoft.AspNetCore.Components.Authorization;
namespace Front.Services
{
    public class AuthService
    {
        private readonly HttpClient httpClient;
        public AuthService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<AuthResponse?> Login(AuthRequest request)
        {
            var result = await httpClient.PostAsJsonAsync("api/auth/login", request);
            var content = await result.Content.ReadAsStringAsync();
            if (result.IsSuccessStatusCode)
            {
                AuthResponse response = JsonSerializer.Deserialize<AuthResponse>(content);
                return response;
            }
            else if(result.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new BadHttpRequestException(content);
            }
            else
            {
                return null;
            }
        }
        public async Task<AuthResponse?> Register(RegisterRequest request)
        {
            var result = await httpClient.PostAsJsonAsync("api/auth/register", request);
            var content = await result.Content.ReadAsStringAsync();

            if (result.IsSuccessStatusCode)
            {
                AuthResponse response = JsonSerializer.Deserialize<AuthResponse>(content);

                return response;
            }
            else if(result.StatusCode == HttpStatusCode.BadRequest)
            {
                throw new BadHttpRequestException(content);
            }
            else
            {
                return null;
            }
        }

        public async Task<AuthResponse> RefreshToken(AuthResponse response)
        {
            if (response.token == null) return null;
            var jwtToken = new JwtSecurityTokenHandler().ReadToken(response.token) as JwtSecurityToken;
            var tokenExp = jwtToken.ValidTo - DateTime.UtcNow;
            Console.WriteLine("Token expxpxpxp " + tokenExp.TotalMinutes);
            if(tokenExp.TotalMinutes < 2 && tokenExp.TotalMinutes > 0)
            {
                Console.WriteLine("Refresh token poststststst");
                var result = await httpClient.PostAsJsonAsync("api/auth/refresh", response);
                if (result.IsSuccessStatusCode)
                {
                    var content = await result.Content.ReadAsStringAsync();
                    AuthResponse res = JsonSerializer.Deserialize<AuthResponse>(content);
                    var jwtTokenf = new JwtSecurityTokenHandler().ReadToken(res.token) as JwtSecurityToken;
                    var tokenExpf = jwtTokenf.ValidTo - DateTime.UtcNow;
                    Console.WriteLine("New token expxpxpxp  " + tokenExpf.TotalMinutes);
                    return res;
                }
                
            }
            else if(tokenExp.TotalMinutes < 0)
            {
                throw new UnauthorizedAccessException();
            }
            
            return null;
        }
        public async Task<string> GetRoleOfUser(string token)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, "api/auth/get-role");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await httpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                return result;
            }
            else if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException();
            }
            return null;
        }
    }
}
