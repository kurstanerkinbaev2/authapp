﻿using Domain.DTO;
using Domain.Models;
using WebApi.Data;

namespace WebApi.Services
{
    public class AuthService : IAuthService
    {
        private readonly JwtService jwtService;
        private readonly UserContext userContext;

        public AuthService(JwtService jwtService, UserContext userContext)
        {
            this.jwtService = jwtService;
            this.userContext = userContext;
        }

        public AuthResponse AuthenticateUser(AuthRequest request)
        {
            var user = userContext.Users.SingleOrDefault(u => u.Username == request.Username);
            if (user == null)
            {
                throw new Exception($"User with name {request.Username} is not found");
            }
            if (!user.Password.Equals(request.Password))
            {
                throw new Exception("Password incorrect");
            }
            var accessToken = jwtService.GenerateAccessToken(user);
            var refreshToken = jwtService.GenerateRefreshToken();
            user.RefreshToken = refreshToken;
            user.RefreshTokenExpiryTime = DateTime.UtcNow.AddMinutes(3);
            userContext.SaveChanges();
            return new AuthResponse
            {
                token = accessToken,
                refreshToken = refreshToken,
            };
        }

        public AuthResponse RefreshToken(AuthResponse tokenDto) 
        {
            string accessToken = tokenDto.token;
            string refreshToken = tokenDto.refreshToken;

            var principal = jwtService.GetPrincipalFromExpiredToken(accessToken);
            var username = principal.Identity.Name;
            var user = userContext.Users.SingleOrDefault(u => u.Username == username);

            if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.UtcNow)
                throw new Exception("Invalid client request");
            var newAccessToken = jwtService.GenerateAccessToken(user);
            var newRefreshToken = jwtService.GenerateRefreshToken();
            user.RefreshToken = newRefreshToken;
            user.RefreshTokenExpiryTime = DateTime.UtcNow.AddMinutes(3);
            userContext.SaveChanges();

            return new AuthResponse
            {
                token = newAccessToken,
                refreshToken = newRefreshToken
            };
        }

        public AuthResponse Register(RegisterRequest request)
        {
            var user = userContext.Users.SingleOrDefault(u => u.Username == request.Username);
            if (user != null)
            {
                throw new Exception($"User with name {request.Username} is already exist");
            }
            User newUser = new User()
            {
                Username = request.Username,
                Password = request.Password,
                Role = request.Role
            };
            var accessToken = jwtService.GenerateAccessToken(newUser);
            var refreshToken = jwtService.GenerateRefreshToken();
            newUser.RefreshToken = refreshToken;
            newUser.RefreshTokenExpiryTime = DateTime.UtcNow.AddMinutes(3);
            userContext.Add(newUser);
            userContext.SaveChanges();
            return new AuthResponse
            {
                token = accessToken,
                refreshToken = refreshToken,
            };
        }
    }
}
