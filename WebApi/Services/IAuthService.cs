﻿using Domain.DTO;

namespace WebApi.Services
{
    public interface IAuthService
    {
        AuthResponse AuthenticateUser(AuthRequest request);
        AuthResponse RefreshToken(AuthResponse tokenDto);
        AuthResponse Register(RegisterRequest request);
    }
}
