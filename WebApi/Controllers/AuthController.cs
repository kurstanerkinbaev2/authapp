﻿using Domain.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private readonly IAuthService authService;

        public AuthController(IAuthService authService)
        {
            this.authService = authService;
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] AuthRequest request)
        {
            try
            {
                return Ok(authService.AuthenticateUser(request));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("refresh")]
        public IActionResult RefreshToken([FromBody] AuthResponse tokenDto)
        {
            try
            {
                return Ok(authService.RefreshToken(tokenDto));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpPost("register")]
        public IActionResult Register([FromBody] RegisterRequest request)
        {
            try
            {
                return Ok(authService.Register(request));
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-role")]
        [Authorize]
        public IActionResult Get()
        {
            return Ok(User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value);
        }

        //[HttpGet]
        //public IActionResult Gethash()
        //{
        //    string dataString = "201STATICb.elcart.kg612572212316182340760124171321531795141string60410";

        //    // Преобразование строки в массив байт с кодировкой UTF-8
        //    byte[] byteArray = Encoding.UTF8.GetBytes(dataString);
        //    string hashString;
        //    // Вычисление хеша массива с использованием алгоритма SHA256
        //    using (SHA256 sha256 = SHA256.Create())
        //    {
        //        byte[] hashBytes = sha256.ComputeHash(byteArray);

        //        // Преобразование массива байт обратно в строку
        //        hashString = BitConverter.ToString(hashBytes).Replace("-", "");

        //        // Получение последних 4 символов
        //        string lastFourChars = hashString.Substring(hashString.Length - 4);

        //        Console.WriteLine(lastFourChars);   
        //    }
        //    return Ok(hashString);
        //}
    }
}
